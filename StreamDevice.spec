%define _prefix /gem_base/epics/support
%define name streamdevice
%define repository gemdev
%define debug_package %{nil}
%define arch %(uname -m)
%define checkout %(git log --pretty=format:'%h' -n 1) 

#These global defines are added to prevent stripping
# symbols on vxWorks cross-compiled code
# Getting 'strip' to work is probably only needed for
# building a related debug sub-package
#
# But this prevents all the strip warnings
# mrippa 20120202
%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

Summary: %{name} Package, StreamDevice is a generic EPICS device support for devices with a byte stream based communication interface.
Name: %{name}
Version: 2.8.22
Release: 0
License: EPICS Open License
Group: Applications/Engineering
Source0: %{name}-%{version}.tar.gz
ExclusiveArch: %{arch}
Prefix: %{_prefix}
## You may specify dependencies here
BuildRequires: epics-base-devel sequencer-devel asyn-devel geminicalc-devel sscan-devel geminipcre-devel
Requires: epics-base sequencer asyn geminicalc sscan geminipcre
## Switch dependency checking off
# AutoReqProv: no

%description
This is the module %{name}.

## If you want to have a devel-package to be generated uncomment the following:
%package devel
Summary: %{name}-devel Package
Group: Development/Gemini
Requires: %{name}
%description devel
This is the module %{name}.

%prep
%setup -q 

%build
make distclean uninstall
make

%install
export DONT_STRIP=1
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r include $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r configure $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r dbd $RPM_BUILD_ROOT/%{_prefix}/%{name}
find $RPM_BUILD_ROOT/%{_prefix}/%{name}/configure -name ".git" -exec rm -rf {} \;


%postun
if [ "$1" = "0" ]; then
	rm -rf %{_prefix}/%{name}
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/%{name}/lib

%files devel
%defattr(-,root,root)
   /%{_prefix}/%{name}/dbd
   /%{_prefix}/%{name}/include
   /%{_prefix}/%{name}/configure

%changelog
* Wed Mar 30 2022 Matt Rippa <matt.rippa@noirlab.edu> 2.8.22-0
- Realign to upstream vendor release 2.8.22. Fix cplusplus linkage error with
--  extern "C". Closes  #6
- Staging vendor release
- fix crash due to length underflow
- add new checksum: bitsum
- make "ifdef CALC" also consider SYNAPPS variable
- make "stream" in DBD and other file names explicit
- fix strncpy warning
- Canonical handling of user include files
- strncasecmp does exist when compiling for Windows on MinGW
- Conditionally use tirpc if defined
- Add  preprocessor definition for static PCRE
- 32 bit Windows needs wrapper for calling convention reasons
- Change log added
- poll for I/O Intr only while interruptAccept is true
- allow to use absolute file paths
- allow : and ; as separators in STREAM_PROTOCOL_PATH
- re-arrange includes: first OS, then other software (e.g. EPICS), then my own
  headers
- shifted noisy debug from parser and I/O Intr to level 2
- print thread name in messages even if time stamps are not printed
- fix initial space when timestamp is off
- document streamMsgTimeStamped
- added a variable 'streamMsgTimeStamped' to enable/disable timestamps on debug
  & error messages
- some rules for contribution added
- remove signed/unsigned warning
- fix initializer which is not compatible with vxWorks 5
- Refactor to set dead time as a shell variable and add documentation
- Added configurable dead time for repeated messages
- Created a generic function for reducing logging frequency of the same error
- Print reply timeout errors only every 5 seconds
- Reply timeouts will now not be printed multiple times
- fix test after introducing ansiEscape() function
- color mode depends on stderr, not stdout
- build for all OS classes
- Added description of the LRC checksum to the documentation
- Implemented lrc checksum and also a version that interprets ASCII as number:
  hexlrc
- Initialize inTerminatorDefined and outTerminatorDefined
- Remove out of date comment
- Add <unistd.h> for non-windows systems
- Remove errlog.h include
- Use streamDebugColored IOC variable instead
- Check for NULL as well as INVALID_HANDLE_VALUE
- Add missing space character
- Allow configuring of coloured console output with STREAM_DEBUG_COLOR
- vxWorks does not support %%hhx scanf format
- fix formats for vxWorks 6.9
- fix some type conversion warning on Windows
- install missing headers
- fix problem with extra spaces after protocol name in link
- Do not use upper level configure and do not install to upper level any more.
  Drops backward compatibility to Stream 2.7
- make version generation more robust
- fix version string in case that no version number is available
- fix Windows branch macro
- Generate version header for zip exports as well
- change quotes for Windows
- generate version file from git tag
- cleanup whitespace
- fix bug affecting EPICS 7 introduced in commit 8000f41
- Link streamApp against seq and pv libs when SNCSEQ is defined
- Remove HOST_OPT=NO
- apply LGPL-3.0 license
- error message on reply timeout
- bugix for systems where vsnprintf returns -1 on buffer overrun: double buffer
  capacity
- bugfix: when passing negative start index to find(), don't start earlier than
  index 0
- allow commas inside matching pairs of parentheses in protocol parameters
- allow () inside protocol parameters
- tests updated
- cleanup spaces
- Fix linker error on WIN32
- fix linker problem on windows
- New Checksums including support for Brooks Cryopump
- update documentation according to previous API change
- Make expectedLength ssize_t everywhere.
- Cast expectedLength to ssize_t where it is used as a signed number. Fixes
  buffer overrun memory corruption segfault.
- Bugfix in 64 bit data type handling. (Thanks to Andrew Johnson)
- remove error message in streamReadWrite
- made RELEASE file more similar to SynApps
- improve building streamApp for 3.13
- do not use epicsStdioRedirect.h for EPICS 3.13
- do not use -ansi with vxWorks compiler in EPICS 3.13
- no need to go into config for 3.13 and it can cause problems with 3.14+
- fix generation of stream-calcout.dbd: older EPICS versions have no CAT macro
  and we don't want duplicate registrations
- support epicsStdioRedirect everywhere
- don't include 3.14 headers in EPICS 3.13
- fix problems with extern C and epics headers in different EPICS versions
- don't complain unnecessarily in finishProtoco() when protocols are reloaded
- Show errors when debug is enabled
- updated docu for EPICS 7.0.3
- have error logging initially enabled
- fix bug that caused endless loops in I/O Intr records
- document recommended value for PollPeriod
- document debug and error messages
- Use $(CAT) for WIN32
- Fix linker import warning for StreamDebugFile on WIN32
- Fix for VS2010 usage of __VA_ARGS__
- Fix missing inttypes.h on VS2010
- Define ENABLE_VIRTUAL_TERMINAL_PROCESSING if not present
- fix for commit cb4d49: needed to add SUPPORT=, not RELEASE= for SynApps
- Have separate dbd file without scalcout support (by Martin Konrad)
- add new record types to the pdf
- add tolower and toupper to regsubst
- Update some EPICS related links
- Support %%%% in protocol just like in printf/scanf. (Suggested by Klemen
  Vodopivec)
- Fix incosistent readRequst() function signature in documentation.
- move enum Commands into class StreamCore to avoid collisions of enum state
  wait with function pid_t wait(int*)
- link streamApp with sscan if SSCAN is defined in RELEASE
- don't mess with asynTraceMask any more
- regsub converter: empty match advances by 1 byte to avoid loops
- fix hanging 'in' command when it is the first command of the protocol and the
  device is offline
- fix broken version detection
- Use inverse instead of fat for hex byte output because not all terminals
  support fat off
- define feature test macro to ensure vsnprintf is available
- Implement needed vsnprintf for vxWorks 5. (vxWorks 6 has it already)
- RTEMS has vsnprintf at least since 4.5.1-pre3
- Windows has vsnprintf at least since 2003
- enable rendering of ANSI term colors on Windows console
- avoid problems with echo and quotes in Windows
- fix problem with redirects to records that do not start with a letter or
  underscore
- fix test: sign extension does not depend on 32 or 64 bit architecture any
  more
- handle state alarms in mbbo update
- fix bug in mbbo string readback
- mbbo changes: use SHFT and MASK even with VAL (if no xxVL defined or ENUM
  format)
- test build with epics 7
- Prep for EPICS R3.15.6 release
- remove STREAM_PATCHLEVEL macro because I always forget to update it anyway
- fix rule to rebuild StreamVersion.o whenever any other object code changed
- added RELEASE variable for SynApps
- Fix problem with \? at end of input format when no more input is available.
  Used to work. Works again (but was not really intended)
- Web link fixes, updated 3.14.8 dev guide link to 3.14.12
- use macro instead of magic value
- improve debug message
- reset proc to 0 in case it had been 2 to trigger @init
- Typos on DFB_ULONG
- Typo
- Do not limit to 80 chars when the link can be 256 by @krisztianloki
- Fixed new[] / delete mismatch by @krisztianloki
- fix (and simplify) reading strings into waveform, aai, aao, lsi, lso
- fix of the fix
- Start search for end parenthesis one character sooner to allow empty
  parentheses
- Minor typo
- Un-breaking EPICS links
- Treat ENUM as signed to allow things like %%#{A=-1|B=0|C=1}
- Update README.md
- Create README.md
- moved documentation to docs/ directory to fit github convention
- bugfix in regsub
- add re-init support to aao
- header usage in record interfaces simplified
- record interface documentation added
- documentation update
- fix several signed/unsigned problems
- improved documentation
- add streamReinit function
- re-init record when 2 is written to .PROC
- fix spurious error messages when asyncronous protocols get aborted (e.g. from
  reload)
- bugfix: ignore space after last protocol argument
- improved error messages
- make monitors trigger when @init updates output records
- update docu
- mention new data types in docu
- description for new record types added
- convert doc to html5
- Update Setup docu page: new configure, github, update links, improve example
- Improve docu about serial device setup
- allow spaces around protocol parameters
- bugfix in reloading protocols
- make StreamBuffer::dump nicer
- bugfix: after streamReload handlers and variables that existed before but do
  not exist any more had not been deleted
- allow redirection of protocol dump from ioc shell
- Fix bug in parser when command reference was followed by } without ;
- re-run @init in iocRun after iocPause and streamReload
- remove noisy debug message
- allow output redirect of shell functions
- use static wrappers instead of friend functions where possible
- new connection handling: run @init at each re-connect
- save name/address of bus interface
- mark hex bytes in debug output
- make streamError settable before iocInit
- Do some macro magic to convert enums to strings
- if write fails because device has disconnected, try to re-connect and re-do
  write once
- clean up error messages
- drop support for buggy cygnus-2.7.2 compiler
- code formatting
- version updated
- added missing output convertion from ULONG in waveform record
- fix bug that leads to infinite callback loop when first command (typically
  'out') fails
- fix for 'skip' and '?' in 'in' command
- allow literal \0 - \9 in regsub if there is no matching sub-expression
- update version
- always enable error messages when loading protocol files
- bugfix: in quoted strings do not mis-interpret double backslash before quote
  or $ as escape char
- buxfix: in regsub continue scanning only after last substitution
- improve build system
- use typedef instead of define
- use localtime_s instead of localtime in Windows
- build with 3.16
- fixed bug in signed octal and hex recogition
- compare performance of different data types
- test aai and aao together with waveform
- simplify most commonly uses test commands
- somewhat better debug output
- fixed bug with arrays introduced in commit
  9f6d4ab63f886e50c1fd1f215d29868d2d320d84
- do not depend on . in PATH for tests
- escape all chars higher than 127 in debug output to <..> to avoid problems
  with unicode terminals
- removed invalid test
- added negative bcd test
- made BCD code compacter
- bug fixed with negative BCD values
- bugfix in %%D: output was 1 byte too short
- allow some time to read the web page
- use types with defined size for checksums
- new checksums added
- new function streamSetLogfile to set debug log file in script or at run-time
- make git ignore log files
- make examples executable from (Linux) command line
- make Makefile's compatible with newer EPICS versions
- increase Windows happyness even more
- remove some old stuff and escape sub-includes from epicsExportSharedSymbols
  to increase happyness of Windows
- put extern "C" around all epicsExport* to increase happyness of Windows
- make script compatible with older sh versions
- fix comments
- remove unneeded headers
- include devStream.h last to increase happyness of Windows
- more type cleanup
- fix warnings on Linux introduced by previous changes on Windows
- change all length agruments and return types to size_t or ssize_t
- change all length agruments and return types to size_t or ssize_t
- bugfix in devscalcoutStream.c
- moved all epicsExport.h to one place
- removed unfiltered debug message
- support for lsi and lso records added
- changed streamScanfN to return number of read bytes
- explicitly test streamPrintf and streamPrintfN for ERROR
- clean up some whitespace
- fix for 3.16+
- clean up some 'extern "C"' calls
- finally fix problems with driver.makefile build using include
  src/CONFIG_STREAM
- support for new 64-bit records
- improve how calcout support is built depending on EPICS version
- fixes for 3.16+
- do not assume any particualr size of VAL field
- minor cosmetics
- made function attributes immune to macros like printf
- for all non-system headers use #include "" instead of include <>
- some cleanup after manual merge
- start Version 2.8
- add new utility function streamReportRecord
- fix bug printing status of unprocessed records
- fix bug in protocol printing
- fix off-by-one error
- Move scalcout support to src dir
- handle new 64 bit data types
- silence C++11 compiler warning
- silence compiler warning
- cleanup header usage for windows
- cosmetic changes
- Do not encode formats during recursion
- Print the mismatching characters to ease debugging
- Avoid repeated messages for errors from asyn layer
- Fixed indentation of a do-while loop in AsynDriverInterface.cc
- update version number
- escape all non-ascii chars to avoid problems with UTF terminals
- Don't change EOS if not necessary. And some improved diagnostics
- do not run exceprion handlers from @init
- fix version number
- add rule to install PDF
- source code repo ignore generated pdf file
- check if wkhtmltopdf is installed, else print message
- fix page break in <pre> boxes
- fix formatting problem
- update documentation
- add examples for 3.14.8 and 3.13.10 to RELEASE
- add standalone stuff for 3.13 build
- Format %%0s pads with null bytes instead of spaces
- for 3.13 compatibility don't use epicsVsnprintf
- remove compiler warning
- Removing conflict at write in devmmboDirectStream.c
- old compilers complain about comma at end of enum list
- some compatibility work
- renamed doc to documentation so that make distclean does not delete it
- fix for previous change
- make compatible to 3.14.8- and remove duplicate code
- move scalcout to src
- fixes for 3.13
- add external modules
- update .gitignore with generated directories
- change macroname to simplify PSI GNUmakefile
- remove compiler warning
- fix standard EPICS build system
- add format error checking to exec command
- fix waveform records printing unsigned integer formats like %%c
- typo in docu fixed
- make compatible to standard EPICS build system
- get isnan and isinf for old 3.13 (vxWorks only) as well as for 3.14+ EPICS
- Windows does not like macros after strings without space
- fix ai/ao conversion when using LINR=NO CONVERSION together with ASLO or AOFF
- New file to ignore generated files
- Use epicsMath.h to define isnan and isinf if needed; would not compile on
  Visual Studio 2010
- Update StreamBuffer.h
- version update
- Disable error messages by default in order to prevent filling the log. Set
  variable streamDebug to 1 to enable error messages.
- typo fixed
- remove unneeded file
- make docu for %%r converer more specific about %%.2r versus %%2r
- workaround for mbboDirect bug
- workaround for mbboDirect bug
- change the meaning of pre for regsub slightly
- version macros updated
- bugfix in regsub
- do not fill log files with lock errors
- regsub converter added
- "No reply from device" error message removed because it only fills the log
  files
- improved error message
- use local variable for fieldBuffer because on some arch (Moxa ARM5) 64 bit
  (double) values are corrupted otherwise
- find clean way to create dbd file
- provide local strncasecmp for 3.13
- version macros updated
- checksums updated
- uppercase
- modbus checksum added
- links updated
- highlinting of new featured removed
- fixed witdth flag ! added
- renamed to make Windows happy
- typo fixed
- Split long_format into signed_format and unsigned_format to hangle sign
  extension from EPICS LONG or ULONG to long or unsigned long correctly on
  systems with 64 bit long.
- fix tests
- limit number of significant half bytes in %%x formats to avoid problems on 64
  bit machines
- add default choice in enum output
- fix redirection of strings to char arrays
- allow and document direct conversion from VAL/OVAL to integer to get more
  bits on 64 bit systems
- fix for 64 bit machines
- find correct object code
- If ai uses no conversion, transport long i/o to and from val directly. This
  preverves more bits on 64 bit systems than going through the 32 bit rval
  field.
- fix for version check
- make it work with old and new version of driver.makefile
- avoid buffer overflow
- check for error condifiton
- debug message improved
- fix message when INP/OUT link is empty fix bug with formatting arrays of char
  as strings in redirect
- Stupid error: Minutes and seconds start from 0, not 1.
- Fix or rather workaround for MinGW and maybe other systems without
  localtime_r function
- function pointer type problem fixed
- script to make a pdf from the hrml pages
- new logo and design cleanup, printing improved
- new logo, png instead of gif, better quality
- shorter header, better for printing
- fix for BSD Linux
- patchlevel fixed
- error messages improved
- write error message on write timeout
- newline in report output fixed
- No connection checking before locking device. Multi-devices (GPIB) behave
  strangely. Get an error when device is disconnected.
- patchlevel updated
- .
- status information added
- status information added error messages improved dont mark device as BusOwner
  when locking fails
- status information added use class for status strings to make array overrun
  impossible
- typo in comment
- error messages improved Do not queue lock request when not connected Do not
  call lock callback when unlock fails
- typo
- Do not connect unrequested. Do not register for int/uint interrupts without
  need.
- stay at version 2.6
- Error Message improved
- fix for cygwin
- fixes for windows
- lock timeout error message added
- version update
- *** empty log message ***
- version updated
- Handle inverted and negative checksums generically. Get rid of ulong and
  uchar typedefs. use unsigned int unstead of ulolg for return types.
- use ssize_t for signed index types. Win has no ssize_t, use ptrdiff_t
  instead. use P instead of S as shortcut for PRINTF_SIZE_T_PREFIX because some
  OK already use S.
- use ssize_t for signed index types. Win has no ssize_t, use ptrdiff_t
  instead.
- changed shortcut for PRINTF_SIZE_T_PREFIX from S to P. Some OS use S already.
- more debug init I/O Intr scanning only if init_record did not fail
- make name() public
- more debug
- use size_t and ssize_t in streamBuffer
- new format: %%-<> prints "poor man's hex": 0x30 - 0x3f characters
- bug in debug output fixed
- allow selecting asyn version
- *** empty log message ***
- fix the fake flush: break early when no bytes are left
- handle errors in flushing read correctly
- missing space ckeck in event command
- problems with 3.13 solved
- Can't poll GPIB.
- Fixes for 3.15
- compiler warning removed
- application developper's guide link updated
- record reference manual link updated
- version release
- version update
- remove warning
- Fixes for 3.15
- avoid multiple inclusion of asyn.dbd
- Adjustments for 3.15: test for 3.13 instead of 3.14
- link pointer is const
- Link parsing in a separate function
- Improved debug message, more digits in debug timestamp.
- Dummy-read stale input instead of flushing it in order to make it available
  for I/O Intr records.
- check if businterface exists before calling its methods
- missing header
- version update
- no eol translation
- Fixes for Windows: dll import and export
- fixes for asyn 4.11+ (queueRequest fails on disconnected ports)
- reading web pages
- anchor for redirections
- always escape special chars and bytes
- Print helpful message when matching string is too long
- Use disconnect by server as a regular terminator (e.g. web pages)
- put patchlevel into version string
- get correct version of stream library
- added constant "%%" test
- high PRIO for command records
- add "binary" command, update help, GUI with exit button.
- rework number parsing. make sure '%%' in any numeric format gets escaped.
- more debug
- use perl script instead of bash commands to create dbd file
- for 3.13 don't create variable and registrar entries
- use a StreamBuffer to hold format string for later error and debug messages
- Skip leading whitespaces only if neither 0 nor 1 are whitespaces.
- debug messages improved
- forgot to update minot version
- implement decimal checksum output
- need to rename StreamBuffer method printf to print because on cris
  architecture printf is a macro.
- calcout support was missing
- remove compiler warning
- allow checksums in decimal ascii
- bugfix: too high array index
- problem with connection handling workaround
- updates
- make stream.dbd in O directories
- not needed
- ignore stream.dbd
- use perl scripts to generate files
- build autimatically
- put in aai and aao, they are now standard parts in 3.14.12
- Modifications for new versions of base, asyn, calc
- use perl script instead of shell script to allow for windows
- dependency stuff added for 3.14.12
- fix for 64 bit machines
- typo
- fixes for 64 bit machines
- Improve re-connection handling in unexpectedly disconnecting IP ports.
- bugfix: this could hang up the record in PACT=1 state
- no portable way to test this
- unfortunately we have to link PCRE here because otherwise static builds fail
- exit command added
- fixes for 64 bit machines
- use correct delete operator
- Workaround for strange bug on xscale_be
- report protocol name with original case
- bugfix: had stale fieldAddress under some circumstances
- must find a way to generate this
- fix endless loop when port does not support EOS
- test reserve()
- input tests added
- don't link to PCRE here, that has been done in stream library already.
- compatibility problem with shell on solaris
- *** empty log message ***
- compatibility problem with "inf" and "Inf" on different archs
- compatibility problem with old tcl version on solaris
- test to check locking and priorities
- Fixes for windows Don't have a bash here so use perl. Need to give the full
  path for PCRE (strange)
- port locking sometimes deadlocks when used together with syncronous software
  and sometimes crashes the timer queue
- windows patch
- avoid compiler warnings
- explicit cast to avoid compiler warning
- xmodem alias added
- Bugfix: escaped %% in format did not work
- Bugfix: allow out formats in exec
- Add dummy interface
- *** empty log message ***
- non-asyn interface without I/O
- Buggy old Tornado compiler (cygnus-2.7.2). Comment out some debug messages.
- allow record name without .VAL in redirection
- make markbit small enough to be inlined on solaris
- don't mention unused parameters to avoid compiler warning
- memguard shell fuction removed from here (may go to a separate file)
- avoid strange gcc warning
- I/O Intr bugfix: lines where lost when not read in one chunk
- no memguard
- add NP byte code as alias for FF
- Bugfix: Infinite recursion with multi-line input and I/O Intr records
- print single space for "arbitrary whitespace" and nothing for "skip"
- use _ to match arbitrary whitespace
- New special char: "_"  means arbitrary amount of whitespace
- Separator re-work did not work
- separator pre-parsing did not work as expected. Problems with whitespace
  separators. Back to old soluton
- Fix for RTEMS
- Bugfixes: escaped quotes in strings and multi-line format strings
- *** empty log message ***
- fixes for new TCL version
- *** empty log message ***
- wrong sign in time zone calculation
- Space handling in separartors and literal input: Any amount of whitespace
  matches space character in litrals and if space is first character of a
  separator.
- several bugfixes: time zone, windows, shortcut formats
- sscanf removed from %%c and %%[]
- obsolete separator function removed
- fixes for new asyn version
- *** empty log message ***
- handle new asyn status codes for asyn 4.11
- diamond version of port locking because of probplems with tpmac driver
- sscanf removed from %%s
- re-work separator parsing. Now first byte of separator is replaced with a
  0x00 byte to allow e.g. simple %%s parsing for an element (instead of e.g.
  %%[^,] )
- ugly goto removed
- allow for negative length (meaning left of start) in expand
- changed from scanf to strto* for parsing
- sscans removed from integer and double scanning because of bad performance
- Preset buffer size removed (It was not the performance killer) Bugfix:
  InTerminator search: distinguisch between long input line and multi-line
  input StreamBuffer::equals renamed to startswith
- StreamBuffer::equals renamed to startswith
- bugfix: reserve returned wrong pointer optimized implementation to append 1
  char operator += aliases for append with 1 argument equals renamed to
  startswith
- bugfix: local buffer was not set to 0 at initialization
- test empty string enum choice
- allow SLS style module loading
- %%f default precision changed to 6 because of limited precision of a double.
- Performance bug fixed: search for terminator was O(n^2)
- errlog.h required for epics 3.14.11
- made grow() public to allow to set initial buffer size
- Initial buffer size added
- crc16 xmodem added
- copy from other repository
- latest features added and bugs fixed: compare %%= timestamp converter %%T
  error and debug messages
- have both, makebaseapp Makefile and PSI makefile
- timestamp converter added
- allow assigned enums: %%#{one=1|two|neg=-1}
- %%m format renamed from Exponential to MantissaExponent
- allow formats in exec
- Allow optional input %%? format
- Find byte order at run-time because there is no standard how to do this at
  compile-time.
- Bugfix: prec too long had corrupted output
- *** empty log message ***
- renamed %%m from Exponention to MantissaExponent
- take care of early input (race condition device reply and start of in
  command)
- install headers to allow plugins (formats) in other projects
- Initial Import
- bugfix: find() did not find 1 char strings
- comments fixed
- avoid compiler warning on 64 bit machines
- increase minor version number
- allow for 64 bit machines
- Use long constant 1L to allow for 64 bit machines
- get rid of writeRaw for asyn 4.10
- on vxworks, return value of sscanf with %%* formats is buggy
- Build calcout only for 3.14
- do record processing in callback tread instead of port thread
- error msg improved
- result strings fixed
- ShowAsyncErrors hack removed
- RawFloat added
- comment fixed
- raw float converter added
- When not connected, call lock request with priority asynQueuePiorityConnect
  to avoid deadlock.
- raw float added
- allow append of n equal bytes
- output format for exponential converter defined
- fix debug output of format string
- %%m added
- comments fixed
- bugfix: 1st byte was lost feature: little endian support
- comment added
- improved error message
- alignment bug (on machines where it matters)
- generate 3.14 dbd file
- don't return DO_NOT_CONVERT (2) from initRecord
- bugfix
- %%B fixed
- %%B fixed
- version update
- better PCRE support
- disconnect callback
- *** empty log message ***
- *** empty log message ***
- better PCRE support
- support signed
- Don't abort on big input
- disconnect callback
- copy from main project
- *** empty log message ***
- update to current snapshot version
- typo
- version 2.2

* Wed Dec 29 2021 Hawi Stecher <hstecher@gemini.edu> 2.7-8
- new package built with tito


